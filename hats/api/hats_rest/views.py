from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import LocationVO, Hat
import json
from common.json import ModelEncoder


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        'fabric',
        'style',
        'color',
        'image_url',
        'location',
        'id',
    ]
    encoders = {
        "location": LocationVOEncoder()
    }

    # def get_extra_data(self, o):
    #     return {"location": o.location.closet_name}


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        'fabric',
        'style',
        'color',
        'image_url',
        'location',
        'id',
    ]
    encoders = {
        "location": LocationVOEncoder()
    }

    # def get_extra_data(self, o):
    #     return {"location": o.location.closet_name}


@require_http_methods(["GET", "POST"])
def list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            # location_href = f'/api/locations/{content['location']}/'
            location_href = f'/api/locations/{location_vo_id}/'
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"error": "LOCATION DOES NOT EXIST ..... sadge message"},
                status=400
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)


@require_http_methods(["GET", "DELETE", "PUT"])
def show_hat(request, location_vo_id=None):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)
        except Hat.DoesNotExist:
            return JsonResponse({"message": 'oofy memer'})
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get(id=content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": 'nice job, you done broke it'})

        Hat.objects.filter(id=id).update(**content)

        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
