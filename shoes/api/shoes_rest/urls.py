from django.urls import path
from .views import list_shoes, shoe_details

urlpatterns = [
    path('shoes/', list_shoes, name='list_shoes'),
    path('shoes/<int:id>/', shoe_details, name='shoe_details'),
    path('bins/<int:bin_vo_id>/shoes/', list_shoes, name="api_list_shoes"),
]
